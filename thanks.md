---
layout: center
permalink: /thanks.html
---

Muchas gracias por tu comentario! Nos pondremos en contacto a la brevedad.

<div class="mt3">
  <a href="{{ site.baseurl }}/" class="button button-blue button-big">Volver</a>
</div>
