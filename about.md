---
layout: page
title: Acerca de Omnia Salud
permalink: /about/
---
Nos encargamos de proveer los servicios de la Historia Clínica Electrónica para la red de Sanatorios de Galeno.

Omnia Salud es un nuevo concepto en Historias Clínicas Electrónicas que brinda su solución 100% en la nube. Rápida, segura y fácil de usar.

<a href="http://omniasalud.com" target="_blank">
<button class="btn white bg-blue">Visitanos!</button>
</a>
