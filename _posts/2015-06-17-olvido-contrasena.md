---
layout:     post
title:      ¿Olvidó su contraseña?
date:       2015-06-17 12:00:00
summary:    ¿Olvidó su contraseña, expiró o simplemente desea cambiarla?
categories: login
---

En caso de no recordar la contraseña de acceso a Omnia Salud, debes presentarte en la __oficina de Admisión del Sanatorio__ para solicitar el blanqueo de la contraseña.
Luego de esto, se te otorgará una nueva contraseña.

<blockquote>
  <p>
    Recordá que la primera vez que ingreses a la Historia Clínica Electrónica luego del blanqueo de la contraseña se te requerirá que la cambies por una nueva.
  </p>
</blockquote>