---
layout:     post
title:      Plan de Contingencia
date:       2015-06-17 12:00:00
summary:    Plan de Contingencia
categories: contingencia
---

Se pondrá en marcha ante una caída de la Historia Clínica Electrónica definida como:
la imposibilidad de acceder a la misma o error sistemático y sostenido al ejecutar una
o más funciones de la aplicación en al menos dos o más terminales (PC)
de sectores diferentes del sanatorio.

<blockquote>
  <p>
    Pasos a seguir:
    <ol>
      <li>Confirmar la falla:</li>
      <ol>
        <li>Acceso incorrecto: verificá que el número de usuario sea el correcto (número de DNI)
        y que no tengas Bloqueo de Mayúsculas activado. En todo caso,
        podés concurrir a admisión para resetear tu clave de acceso.</li>
        <li>Utilizar otra PC (preferentemente en otro sector del sanatorio)</li>
      </ol>
      <li>Dar aviso a Supervisión de enfermería, que procederá a confirmar la falla intentando ingresar a la HCE
      con su usuario y en caso de no lograrlo procederá a:</li>
      <ol>
        <li>Comunicarse con sistemas a través del conmutador</li>
        <li>Enviar correo electrónico a sistemas</li>
        <li>Registrar horario en el que se intenta la primera comunicación con sistemas</li>
      </ol>
      <li>Sistemas procederá a establecer un diagnóstico de situación con dos posibilidades:</li>
      <ol>
        <li>la falla requiere mas de 40 minutos para ser solucionada o se desconoce el tiempo requerido = iniciar contingencia en papel</li>
        <li>el inconveniente se solucionará en menos de 40 minutos y no es necesario iniciar contingencia en papel (siempre que esto realmente ocurra)</li>
      </ol>
      <li>Supervisión de enfermería deberá registrar el horario de normalización del sistema</li>
    </ol>
  </p>
</blockquote>
