---
layout:     post
title:      Obtener un usuario de Omnia Salud
date:       2015-06-17 12:00:00
summary:    No tengo usuario aún ¿Cómo obtener uno?
categories: login
---

Para obtener tu usuario para la Historia Clínica Electrónica debes presentarte en la oficina de Admisión del Sanatorio y completar la documentación requerida, y firmar un acuerdo de confidencialidad.

<blockquote>
  <p>
    Todos los profesionales de la salud (médicos, enfermeros y auxiliares) pertenecientes al sanatorio y médicos externos podrán solicitar el acceso. También el personal administrativo podrá obtener un acceso a la historia clínica.
  </p>
</blockquote>
